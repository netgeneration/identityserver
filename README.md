### Build
```pwsh
docker build -t identity-server -f ./.ci/Dockerfile ./src/IdentityServer
```

### Seed
```pwsh
docker run -p 3080:80 --volume ${pwd}/src/IdentityServer/DB:/app/DB identity-server --args /seed
```

### Run
```pwsh
docker run -p 3080:80 --volume ${pwd}/src/IdentityServer/DB:/app/DB identity-server 
```

### Browse
http://localhost:3080/Account/Login



## Настройка доступа к приватному докер-реестру

### Добавить креды

```bash
kubectl --namespace <YOUR_NAMESPACE> \
  create secret docker-registry registry-secret \
  --docker-server=https://registry.gitlab.com/ \
  --docker-username=<USERNAME> \
  --docker-password=<PASSWORD>
```

### Скопировать из другого пространства имен

```bash
kubectl get secret registry-secret --namespace=<SOURCE_NAMESPACE> --export -o yaml | kubectl apply --namespace=<TARGET_NAMESPACE> -f -
```